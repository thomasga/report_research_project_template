"""
run model_lorenz.py
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import math
from pathlib import Path

here = Path(__file__).absolute().parent


######## This part of the code came from Wikipedia (a little bit change) ########
rho = 28.0
sigma = 10.0
beta = 8.0 / 3.0

def f(state, t):
    x, y, z = state  # Unpack the state vector
    return sigma * (y - x), x * (rho - z) - y, x * y - beta * z  # Derivatives

state0 = [1.0, 1.0, 1.0]
t = np.arange(0.0, 40.0, 0.01)

states = odeint(f, state0, t)

fig = plt.figure()
ax = fig.gca(projection="3d")
ax.plot(states[:, 0], states[:, 1], states[:, 2])
ax.set_xlabel("$X$")
ax.set_ylabel("$Y$")
ax.set_zlabel("$Z$")

ax.set_title("Potential for the Lorenz model")
plt.draw()
plt.show()


######## I write this part ######
### Fixed points ###
X1=0
Y1=0
Z1=0

if rho>1:
    X2=math.sqrt(beta*(rho-1))
    Y2=X2
    Z2=rho-1
    
    X3=-math.sqrt(beta*(rho-1))
    Y3=X3
    Z3=rho-1

#### stability ####
###fixed point 1###
J1=[[-sigma , sigma , 0],[rho , -1 , 0],[0,0,-beta]]
valJ1, vecJ1 = np.linalg.eig(J1)
if valJ1[0].real<0 and valJ1[1].real<0 and valJ1[2].real<0:
     print('fixed point 1 stable')
else:
    print('fixed point 1 unstable')
    
if rho>1:
    ####fixed point 2###
    J2=[[-sigma , sigma , 0],[1 , -1 , math.sqrt(beta*(rho-1))],[math.sqrt(beta*(rho-1)),math.sqrt(beta*(rho-1)),-beta]]
    valJ2, vecJ2 = np.linalg.eig(J2)
    if valJ2[0].real<0 and valJ2[1].real<0 and valJ2[2].real<0:
        print('fixed point 2 stable')
    else:
        print('fixed point 2 unstable')
   

    ####fixed point 3###
    J3=[[-sigma , sigma , 0],[1 , -1 , -math.sqrt(beta*(rho-1))],[-math.sqrt(beta*(rho-1)),-math.sqrt(beta*(rho-1)),-beta]]
    valJ3, vecJ3 = np.linalg.eig(J3)
    
    if valJ3[0].real<0 and valJ3[1].real<0 and valJ3[2].real<0:
        print('fixed point 3 stable')
    else:
        print('fixed point 3 unstable')
   
tmp_dir = here.parent / "fig"
tmp_dir.mkdir(exist_ok=True)

fig.savefig(tmp_dir / "Lorenz_model.png")
