
"""
run job_sim_verhulst.py
"""
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from mpl_toolkits.mplot3d import Axes3D
import math
here = Path(__file__).absolute().parent

a = 2
b = 1


def f(state, t):
    y = state  # Unpack the state vector
    return  a*y*(1-b/a*y)  # Derivatives

state0 = 1
t = np.arange(0.0, 40.0, 0.01)

y = odeint(f, state0, t)


fig = plt.figure()
plt.plot(t,y)
plt.ylabel('time',fontsize=15)
plt.xlabel('size of teh population Y',fontsize=15)
plt.title('potential of the Verhulst model', fontsize=15)
plt.show
print (a/b)

tmp_dir = here.parent / "fig"
tmp_dir.mkdir(exist_ok=True)

fig.savefig(tmp_dir / "Verhulst_model_Yinf.png")


a = 2
b = 1


state0 = 3
t = np.arange(0.0, 40.0, 0.01)

y = odeint(f, state0, t)


fig = plt.figure()
plt.plot(t,y)
plt.ylabel('time',fontsize=15)
plt.xlabel('size of teh population Y',fontsize=15)
plt.title('potential of the Verhulst model', fontsize=15)
plt.show
print (a/b)

tmp_dir = here.parent / "fig"
tmp_dir.mkdir(exist_ok=True)

fig.savefig(tmp_dir / "Verhulst_model_Ysup.png")
